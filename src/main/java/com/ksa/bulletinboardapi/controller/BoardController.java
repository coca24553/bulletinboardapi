package com.ksa.bulletinboardapi.controller;

import com.ksa.bulletinboardapi.model.BoardBaseInfoChangeRequest;
import com.ksa.bulletinboardapi.model.BoardItem;
import com.ksa.bulletinboardapi.model.BoardRequest;
import com.ksa.bulletinboardapi.model.BoardResponse;
import com.ksa.bulletinboardapi.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/Board")
public class BoardController {
    private final BoardService boardService;

    @PostMapping("/new")
    public String setBoard(@RequestBody BoardRequest request) {
        boardService.setBoard(request);

        return "ok";
    }
    @GetMapping("/all")
    public List<BoardItem> getBoards() {
        return boardService.getBoards();
    }

    @GetMapping("/detail/{id}")
    public BoardResponse getBoard(@PathVariable long id) {
        return boardService.getBoard(id);
    }

    @PutMapping("/base-info/{id}")
    public String putBoardTypeEnum(@PathVariable long id, @RequestBody BoardBaseInfoChangeRequest request) {
        boardService.putBaseInfo(id, request);

        return "ok";
    }

    @DeleteMapping("/{id}")
    public String delBoard(@PathVariable long id) {
        boardService.delBoard(id);

        return "ok";
    }
}
