package com.ksa.bulletinboardapi.entity;

import com.ksa.bulletinboardapi.enums.BoardTypeEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String boardId;

    @Column(nullable = false)
    private String boardName;

    @Column(nullable = false)
    private String boardContent;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private BoardTypeEnum boardTypeEnum;

    @Column(nullable = false)
    private LocalDate createDay;

    @Column(nullable = false)
    private String createName;
}
