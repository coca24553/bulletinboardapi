package com.ksa.bulletinboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BoardTypeEnum {
    QUESTION("질문"),
    CHAT("수다"),
    INFORMATION("정보");

    private final String name;
}
