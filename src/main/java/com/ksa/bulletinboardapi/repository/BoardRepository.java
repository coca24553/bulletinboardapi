package com.ksa.bulletinboardapi.repository;

import com.ksa.bulletinboardapi.entity.Board;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {
}
