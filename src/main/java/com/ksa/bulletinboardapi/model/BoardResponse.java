package com.ksa.bulletinboardapi.model;

import com.ksa.bulletinboardapi.enums.BoardTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardResponse {
    private Long id;
    private String boardId;
    private String boardName;
    private String boardContent;
    private String boardTypeEnum;
    private LocalDate createDay;
    private String createName;
}
