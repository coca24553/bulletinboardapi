package com.ksa.bulletinboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardBaseInfoChangeRequest {
    private String boardName;
    private String createName;
}
