package com.ksa.bulletinboardapi.model;

import com.ksa.bulletinboardapi.enums.BoardTypeEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardRequest {
    private String boardId;

    private String boardName;

    private String boardContent;

    @Enumerated(value = EnumType.STRING)
    private BoardTypeEnum boardTypeEnum;

    private LocalDate createDay;

    private String createName;
}
