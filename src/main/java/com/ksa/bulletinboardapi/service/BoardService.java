package com.ksa.bulletinboardapi.service;

import com.ksa.bulletinboardapi.entity.Board;
import com.ksa.bulletinboardapi.model.BoardBaseInfoChangeRequest;
import com.ksa.bulletinboardapi.model.BoardItem;
import com.ksa.bulletinboardapi.model.BoardRequest;
import com.ksa.bulletinboardapi.model.BoardResponse;
import com.ksa.bulletinboardapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(BoardRequest request) {
        Board addData = new Board();
        addData.setBoardId(request.getBoardId());
        addData.setBoardName(request.getBoardName());
        addData.setBoardContent(request.getBoardContent());
        addData.setBoardTypeEnum(request.getBoardTypeEnum());
        addData.setCreateDay(request.getCreateDay());
        addData.setCreateName(request.getCreateName());

        boardRepository.save(addData);
    }

    public List<BoardItem> getBoards() {
        List<Board> originList = boardRepository.findAll();

        List<BoardItem> result = new LinkedList<>();

        for (Board board : originList) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setBoardId(board.getBoardId());
            addItem.setBoardName(board.getBoardName());
            addItem.setBoardTypeEnum(board.getBoardTypeEnum().getName());
            addItem.setCreateDay(board.getCreateDay());
            addItem.setCreateName(board.getCreateName());

            result.add(addItem);
        }
        return result;
    }

    public BoardResponse getBoard(long id) {
        Board originData = boardRepository.findById(id).orElseThrow();

        BoardResponse response = new BoardResponse();
        response.setId(originData.getId());
        response.setBoardId(originData.getBoardId());
        response.setBoardName(originData.getBoardName());
        response.setBoardContent(originData.getBoardContent());
        response.setBoardTypeEnum(originData.getBoardTypeEnum().getName());
        response.setCreateDay(originData.getCreateDay());
        response.setCreateName(originData.getCreateName());

        return response;
    }

    public void putBaseInfo(long id, BoardBaseInfoChangeRequest request) {
        Board originData = boardRepository.findById(id).orElseThrow();
        originData.setBoardName(request.getBoardName());
        originData.setCreateName(request.getCreateName());

        boardRepository.save(originData);
    }

    public void delBoard(long id) {
        boardRepository.deleteById(id);
    }
}
